import React, { Component } from 'react';
import '../../lib/bootstrap-4.1/css/bootstrap.min.css';
import GuessCount from '../GuessCount';
import KeyboardLetter from '../KeyboardLetter';
import WordLetter from '../WordLetter';
import Restart from '../Restart';

const DICTIONARY = Array.from(['REACT', 'HANGMAN', 'JAVASCRIPT']);
const ALPHABET = Array.from('ABCDEFGHIJKLMONPQRSTUVW');

class Main extends Component {
  constructor() {
      super();
      this.state = {
        wordLetters: Array.from(DICTIONARY[Math.floor(Math.random() * DICTIONARY.length)]),
        guesses: 0,
        matchedLetters: [],
        unmatchedLetters: [],
      };
  }

  resetGame = () => {
    const newWord = Array.from(DICTIONARY[Math.floor(Math.random() * DICTIONARY.length)]);
    this.setState({guesses: 0, matchedLetters: [], unmatchedLetters: [], wordLetters: newWord});
  }

  handleClick = (index) => {
    const {wordLetters, guesses, matchedLetters, unmatchedLetters} = this.state;
    if (wordLetters.includes(index)) {
      matchedLetters.push(index);
      this.setState({matchedLetters: [...matchedLetters]})
    } else {
      const newGuesses = guesses + 1;
      this.setState({guesses: newGuesses});
      unmatchedLetters.push(index);
      this.setState({unmatchedLetters: [...unmatchedLetters]});
    }
  }

  getFeedbackForKeyboard(letter) {
    const {matchedLetters, unmatchedLetters} = this.state;
    const usedLetters = [...matchedLetters, ...unmatchedLetters];
    return usedLetters.includes(letter) ? true : false;
  }

  render() {
    const {wordLetters, guesses, matchedLetters} = this.state;
    const won = matchedLetters.length === wordLetters.length;
    const lost = guesses > 6;
    console.log(this.state);
    return (
      <div className="container">
        <h1>Hangman</h1>
        <GuessCount guesses={guesses} />
        {(won || lost) && <Restart message={won ? 'win' : 'lose'} onClick={this.resetGame}/>}
        <div className="jumbotron">
          <div className="row justify-content-center">
            {wordLetters.map((letter, index) => (
              <WordLetter
                key={index}
                letter={letter}
                matchedLetters={matchedLetters}
              />
            ))}
          </div>

          <hr className="my-4" />

          <div className="row justify-content-center">
            {ALPHABET.map((letter, index) => (
              <KeyboardLetter
                key={index}
                letter={letter}
                feedback={this.getFeedbackForKeyboard(letter)}
                onClick={this.handleClick}
              />
            ))}
          </div>
        </div>

      </div>
    );
  }
}

export default Main;
