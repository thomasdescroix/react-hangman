import React from 'react';
import PropTypes from 'prop-types';

const KeyboardLetter = ({ letter, onClick, feedback}) => {
  return (
    <button disabled={feedback} className="btn btn-primary m-1 text-monospace" onClick={() => onClick(letter)}>{letter}</button>
  )
}

KeyboardLetter.propTypes= {
  letter:PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
}

export default KeyboardLetter;
