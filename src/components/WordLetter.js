import React from 'react';
import PropTypes from 'prop-types';

const HIDDEN_SYMBOL = "_";

const WordLetter = ({ letter, matchedLetters }) => (
  <span className="h1 m-1 text-monospace">
    {matchedLetters.includes(letter) ? letter : HIDDEN_SYMBOL}
  </span>
);

WordLetter.propTypes = {
  matchedLetters: PropTypes.array.isRequired,
  letter: PropTypes.string.isRequired
}

export default WordLetter;
