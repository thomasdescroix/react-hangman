import React from 'react';
import PropTypes from 'prop-types';

const GuessCount = ({ guesses }) => (
    <img src={`./images/hangman_${guesses}.svg`} className="img-fluid mx-auto d-block" alt={`hangman-attempt-${guesses}`} />
);

GuessCount.propTypes= {
  guesses:PropTypes.number.isRequired,
}

export default GuessCount;
