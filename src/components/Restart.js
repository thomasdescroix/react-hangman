import React from 'react';
import PropTypes from 'prop-types';

const Restart = ({ message, onClick }) => (
  <div className={message === 'win' ? "alert alert-success" : "alert alert-danger"} role="alert">
    You {message}!!! <button className="btn btn-link alert-link p-0" onClick={() => onClick()}>Restart?</button>
  </div>
);

Restart.propTypes= {
  message:PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
}

export default Restart;
